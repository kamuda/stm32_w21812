/*
 * ws2812_lib.c
 *
 *  Created on: Dec 1, 2019
 *      Author: skm
 */

// === Includes ===
#include "ws2812_lib.h"


// === Static variables ===
static struct{
	uint8_t dma_buf[WS2812_DMA_BUF_LEN+5]; // < SPI DAM Buff
	uint32_t pixel[WS2812_NUMBER_OF_LEDS];
	uint8_t brightness;	 // < brightness (percentage - values in [0-100])
}ws2812_dev;


// === Static functions ===
static void ws2812_color_to_dma(uint8_t n, uint32_t color);
static void ws2812_to_dma(uint8_t *ptr, uint8_t value);


// === Declarations ===
void ws2812_setBrightness(uint8_t br){
	if(br>100)
		br=100;
	ws2812_dev.brightness = br;
}


uint8_t ws2812_getBrightness(void){
	return ws2812_dev.brightness;
}

void ws2812_setPixelColor(uint8_t n, uint8_t red, uint8_t green, uint8_t blue){
	if(n >= WS2812_NUMBER_OF_LEDS){
		return;
	}
	ws2812_dev.pixel[n] = (((uint32_t)red) << 16)
						| (((uint32_t)green) << 8)
						| (((uint32_t)blue));
}

void ws2812_setPixelColor32(uint8_t n, uint32_t color){
	if(n >= WS2812_NUMBER_OF_LEDS){
		return;
	}
	ws2812_dev.pixel[n] = color;
}

uint32_t ws2812_getPixelColor(uint8_t n){
	if(n >= WS2812_NUMBER_OF_LEDS){
		return 0;
	}
	return ws2812_dev.pixel[n];
}

void ws2812_clear(void){
	for(int i=0; i < WS2812_NUMBER_OF_LEDS; i++)
		ws2812_dev.pixel[i] = 0;
}

void ws2812_render(void){
	for(int i=0; i < WS2812_NUMBER_OF_LEDS; i++)
		ws2812_color_to_dma(i, ws2812_dev.pixel[i]);
}

void ws2812_init(void){
	ws2812_dev.brightness=100;
	ws2812_clear();
	ws2812_render();
	ws2812_dev.dma_buf[WS2812_DMA_BUF_LEN] = 0;
	ws2812_dev.dma_buf[WS2812_DMA_BUF_LEN+1] = 0xff;
	ws2812_dev.dma_buf[WS2812_DMA_BUF_LEN+2] = 0xff;
	ws2812_dev.dma_buf[WS2812_DMA_BUF_LEN+3] = 0xff;
	ws2812_dev.dma_buf[WS2812_DMA_BUF_LEN+4] = 0xff;
}

uint8_t* ws2812_getDmaPtr(void){
	return ws2812_dev.dma_buf;
}

uint8_t ws2812_getDmaSize(void){
	return WS2812_DMA_BUF_LEN+1;
}


static void ws2812_color_to_dma(uint8_t n, uint32_t color){
	uint16_t px_offset = n*9;
	if((px_offset + 9) > WS2812_DMA_BUF_LEN)
		return;
	ws2812_to_dma(ws2812_dev.dma_buf + px_offset,
			((color>>8) & 0xff) * (ws2812_dev.brightness * 0.01));
	px_offset += 3;
	ws2812_to_dma(ws2812_dev.dma_buf + px_offset,
			((color>>16) & 0xff) * (ws2812_dev.brightness * 0.01));
	px_offset += 3;
	ws2812_to_dma(ws2812_dev.dma_buf + px_offset,
			(color & 0xff) * (ws2812_dev.brightness * 0.01));
}





static void ws2812_to_dma(uint8_t *ptr, uint8_t value){
	ptr[0]=0x92;
	ptr[1]=0x49;
	ptr[2]=0x24;
	if(value & 0x80)
		ptr[0] |= (1<<6);
	if(value & 0x40)
		ptr[0] |= (1<<3);
	if(value & 0x20)
		ptr[0] |= (1<<0);
	if(value & 0x10)
		ptr[1] |= (1<<5);
	if(value & 0x08)
		ptr[1] |= (1<<2);
	if(value & 0x04)
		ptr[2] |= (1<<7);
	if(value & 0x02)
		ptr[2] |= (1<<4);
	if(value & 0x01)
		ptr[2] |= (1<<1);
}
