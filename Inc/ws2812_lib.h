#ifndef _WS2812_LIB_DEF
#define _WS2812_LIB_DEF

#include "stm32f1xx_hal.h"

#define WS2812_NUMBER_OF_LEDS 16
#define WS2812_DMA_BUF_LEN 144  //(WS2812_NUMBER_OF_LEDS*9)
#define WS2812_DMA_BUF_size 146  //(WS2812_NUMBER_OF_LEDS*9)

void 	 ws2812_clear(void);
void 	 ws2812_render(void);
void 	 ws2812_init(void);
uint8_t* ws2812_getDmaPtr(void);
uint8_t  ws2812_getDmaSize(void);
void 	 ws2812_setBrightness(uint8_t br);
uint8_t  ws2812_getBrightness(void);
void 	 ws2812_setPixelColor(uint8_t n, uint8_t red, uint8_t green, uint8_t blue);
uint32_t ws2812_getPixelColor(uint8_t n);
void 	 ws2812_setPixelColor32(uint8_t n, uint32_t color);

#endif
